//npm i --save-dev precss gulp-postcss gulp-clean-css gulp-autoprefixer gulp-rename gulp-connect gulp-livereload gulp-uglify gulp-sourcemaps
var	gulp = require('gulp'),
	postcss = require('gulp-postcss'),
	precss = require('precss'),
	calc = require('postcss-calc'),
	minifycss = require('gulp-clean-css'),
	autoprefixer = require('gulp-autoprefixer'),
	rename = require('gulp-rename'),
	connect = require('gulp-connect'),
	livereload = require('gulp-livereload'),
	uglify = require('gulp-uglify'),
	swig = require('gulp-swig'),
	sourcemaps = require('gulp-sourcemaps');

var site = 'public',
	root = 'src',
	htmlSrc = 'src/views/*.html',
	cssSrc = 'src/**/*.css',
	inputCss = 'src/css/*.css',
	outputCss = 'public/assets/css/source',
	outputMinCss = 'public/assets/css',
	inputJs = 'src/js/*.js',
	outputMinJs = 'public/assets/js';

gulp.task('connect', function() {
	connect.server({
		root: '',
		livereload: true
	});
});

gulp.task('html', function() {
	return gulp.src(htmlSrc)
	.pipe(swig({defaults: { cache: false }}))
	.pipe(gulp.dest(site))
	.pipe(connect.reload());
});

gulp.task('styles', function() {
	var packs = [
		precss,
		calc
	];
	gulp.src(inputCss)
	.pipe(postcss(packs))
	.pipe(autoprefixer('last 99 version'))
	.pipe(gulp.dest(outputCss));
});

gulp.task('cssmin', function() {
	gulp.src(outputCss+'/*.css')
    .pipe(rename({suffix: '.min'}))
    .pipe(sourcemaps.init())
    .pipe(minifycss())
    .pipe(sourcemaps.write())
    .pipe(gulp.dest(outputMinCss))
    .pipe(connect.reload());
});

gulp.task('js', function() {
  return gulp.src(inputJs)
    .pipe(uglify())
    .pipe(rename({suffix: '.min'}))
    .pipe(gulp.dest(outputMinJs))
    .pipe(connect.reload());
});

gulp.task('watch', function() {
	gulp.watch(inputJs, ['js']);
	gulp.watch(outputCss+'/*.css', ['cssmin']);
	gulp.watch(inputCss, ['styles']);
	gulp.watch(cssSrc, ['styles']);
	gulp.watch(root+'/**/*.html', ['html']);
});

gulp.task('default', ['connect', 'html', 'styles', 'cssmin', 'js', 'watch'], function() {

});